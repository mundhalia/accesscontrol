<?php

namespace Mundhalia\AccessControl;

use App\Model\Inductee;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class AccessControl
{

    public function createUser($inductee)
    {
        $string = "P|".$inductee->credential."|".$inductee->type."|".$inductee->username."|".$inductee->access->first()->group_name."|".$inductee->email."|".$inductee->mobile."|".Carbon::parse($inductee->activation_datetime)->format('dmy')."|".Carbon::parse($inductee->activation_datetime)->format('Hi')."|".Carbon::parse($inductee->expiry_datetime)->format('dmy')."|".Carbon::parse($inductee->expiry_datetime)->format('Hi');
        return $this->storage($inductee->fileName, $string);

        //$this->sendMessage("Access Code: $key", $mobile);
        //$user->notify(new KeyGeneratedNotification($user));
    }

    public function keyGenerator()
    {
        //Schema::disableForeignKeyConstraints();User::truncate();Schema::enableForeignKeyConstraints();
        $totalKeys = config('accesscontrol.access_key.end') - config('accesscontrol.access_key.start');
        if(Inductee::count() <= $totalKeys){
            do {
                $key = mt_rand(config('accesscontrol.access_key.start'), config('accesscontrol.access_key.end'));
            } while (Inductee::where('credential', $key)->count() > 0);
            return $key;
        }
        else{
            return "No keys avaliable";
        }
    }

    public function deleteUser($inductee)
    {
        $string = "U,".$inductee->credential.",DELETE";
        $this->storage($inductee->fileName, $string);
        return true;
    }

    public function openDoor($door)
    {
        $string = "R,$door->location_number,$door->controller_number,$door->door_number";
        $this->storage('share/LOCK.DAT', $string);
        return true;
    }

    private function storage($fileName, $string)
    {
        if(Storage::disk('local')->exists($fileName)){
            try{
                Storage::disk('local')->append($fileName, $string);
                return true;
            }catch(\Execption $e){
                return $e;
            }
        }
        else{
            try{
                Storage::disk('local')->append($fileName, $string);
                return true;
            }catch(\Execption $e){
                return $e;
            }
        }
    }
}