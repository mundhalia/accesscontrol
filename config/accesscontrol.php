<?php

return [
    'access_key' => [
        'start' => env('ACCESS_KEY_START', 20000),
        'end'   => env('ACCESS_KEY_END', 50000),
    ],
    'name' => 'Access Control System',
    'sms' => [
        'sid'         => env('TWILIO_SID'),
        'auth_token'  => env('TWILIO_AUTH_TOKEN'),
        'number'      => env('TWILIO_NUMBER'),
    ],
    'email' => [
        'from_email' => env('ACCESS_CONTROL_FROM_EMAIL'),
        'from_email_name' => env('ACCESS_CONTROL_FROM_EMAIL_NAME'),
    ],
];